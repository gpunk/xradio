/*for the graphics... */
#include<xview/generic.h>
#include<xview/xview.h>
#include<xview/panel.h>
#include<xview/frame.h>
/*for the hardware - radio card! */
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <asm/io.h>

void r_out(int,int);
void radioon();
void radioff();
void volumeup();
void volumedn();
void do_it(Panel_item,int,Event*) ;
void quit();
void OpenR();
void CloseR();
void DownR();
void UpR();
void TdownR();
void TupR();
void about();
Panel_item state;
#define FCODE(f)	((int)(((float)(f)-88.0)*40)+0xf6c)
Frame frame,abframe;
double current=941;
Panel panel,abpanel;
int status=0;
int main(int argc, char * argv[])
{
  
  Panel panel;
  
  Panel_item slider;
  
  xv_init(XV_INIT_ARGC_PTR_ARGV,&argc,argv,NULL);
  
  frame = (Frame) xv_create((int)NULL,FRAME,FRAME_LABEL,"*****The Xradio***** ",FRAME_SHOW_FOOTER,TRUE,
			    FRAME_LEFT_FOOTER,"done by ->",FRAME_RIGHT_FOOTER,"ESSAISSI Rémi",XV_WIDTH,470,XV_HEIGHT,100,NULL);
  
  panel = (Panel) xv_create(frame,PANEL,NULL);
  
  (void) xv_create(panel,PANEL_BUTTON,PANEL_LABEL_STRING,"quit",
		   PANEL_NOTIFY_PROC,quit,NULL);
  
  (void) xv_create(panel,PANEL_BUTTON,PANEL_LABEL_STRING,"ON!",
		   PANEL_NOTIFY_PROC,OpenR,NULL);
  
  (void) xv_create(panel,PANEL_BUTTON,PANEL_LABEL_STRING,"OFF!",
		   PANEL_NOTIFY_PROC,CloseR,NULL); 
  
  (void) xv_create(panel,PANEL_BUTTON,PANEL_LABEL_STRING,"vol down ",
		   PANEL_NOTIFY_PROC,DownR,NULL); 
  
  (void) xv_create(panel,PANEL_BUTTON,PANEL_LABEL_STRING,"vol up    ",
		   PANEL_NOTIFY_PROC,UpR,NULL); 
  
  (void) xv_create(panel,PANEL_BUTTON,PANEL_LABEL_STRING,"tune - ",
		   PANEL_NOTIFY_PROC,TdownR,NULL); 
  
  (void) xv_create(panel,PANEL_BUTTON,PANEL_LABEL_STRING,"tune + ",
		   PANEL_NOTIFY_PROC,TupR,NULL); 


  
  slider =xv_create(panel,PANEL_SLIDER,PANEL_LABEL_STRING,"station",PANEL_MIN_VALUE,880,PANEL_MAX_VALUE,1080,
		    PANEL_NOTIFY_PROC,do_it,PANEL_VALUE,(int)current,PANEL_NOTIFY_LEVEL,PANEL_ALL,
		    PANEL_SHOW_RANGE,TRUE,PANEL_TICKS,20,PANEL_SLIDER_WIDTH,280,NULL);
  
  
  state = xv_create(panel,PANEL_MESSAGE,PANEL_LABEL_STRING,"What u waiting for ? chrismas!!",NULL);
  
  
  abframe = (Frame) xv_create(frame,FRAME_CMD,FRAME_LABEL,"ESSAISSI",FRAME_SHOW_FOOTER,TRUE,FRAME_LEFT_FOOTER,"i'm happy!! to write this\n
in here!",XV_WIDTH,470,XV_HEIGHT,25,NULL);  
  abpanel = (Panel) xv_get(abframe,FRAME_CMD_PANEL,NULL);  
  
  (void)            xv_create(panel,PANEL_BUTTON,PANEL_LABEL_STRING,"the about!",
			      PANEL_NOTIFY_PROC,about,NULL); 
  
  
  xv_main_loop(frame);
  
  exit(0);
}

void about(Frame item, Event *ev)
{
  xv_set(abframe,XV_SHOW,TRUE,NULL);

}
void quit()
{
  CloseR();
  xv_destroy_safe(frame);
  
}
void TupR()
{
  if (status==0) 
    {
      xv_set(state,PANEL_LABEL_STRING,"turn it on first!!",NULL);
    }
  else
    {
      current=current+1.25;
      radioon(FCODE(current/10),0xa0);
    }
}
void TdownR()
{
  if (status==0) 
    {
      xv_set(state,PANEL_LABEL_STRING,"turn it on first!!",NULL);
    }
  else
    {
      
      current=current-1.25;
      radioon(FCODE(current/10),0xa0);
      
    }
}
void OpenR()
{
  ioperm(0x20f,2,1);
  radioon(FCODE(current/10),0xa0);
  
  xv_set(state,PANEL_LABEL_STRING,"it's on ,u know?!",NULL);
  status =1;
}
void CloseR()
{
  ioperm(0x20f,2,1);
  radioff();
  
  xv_set(state,PANEL_LABEL_STRING," it's off ,hehe! ",NULL);
  status=0;
}
void UpR()
{
  if (status==0) 
    {
      xv_set(state,PANEL_LABEL_STRING,"turn it on first!!",NULL);
    }
  else
    {
  
      volumeup();
    }
  
}  
void DownR()
{
  if (status==0) 
    {
      xv_set(state,PANEL_LABEL_STRING,"turn it on first!!",NULL);
    }
  else
    {
  
      volumedn();
     } 
  
}  
void radioon(int ch,int p2)
{
  r_out(ch,16);
  r_out(p2,8);
  usleep(1000);
  outb(0,0x20f);
  usleep(50000);              /* IMPORTANT to avoid clicks */
  outb(0xc8,0x20f);
}


void radioff(void)
{
  r_out(FCODE(88.0),16);
  r_out(0xa0,8);
  outb(0,0x20f);
  outb(0xc0,0x20f);
}


void volumeup(void)
{
  outb(0x88,0x20f);
  usleep(200000);
  outb(0xc8,0x20f);
}


void volumedn(void)
{
  outb(0x48,0x20f);
  usleep(200000);
  outb(0xc8,0x20f);
}

void r_out(int v,int n)
{
  while (n--) {
    if (v&1) {
      outw (5,0x20f);
      outw (5,0x20f);
      outw (7,0x20f);
      outw (7,0x20f);
    }
    else {
      outw (1,0x20f);
      outw (1,0x20f);
      outw (3,0x20f);
      outw (3,0x20f);
    }
    v>>=1;
  }
}

void do_it(Panel_item slider ,int value, Event *ev)
{
  
  if(status==0)
    {
      xv_set(state,PANEL_LABEL_STRING,"turn it on first!!",NULL);
    }
  else
    {
    
      current=value;
      radioon(FCODE(current/10),0xa0); 
    
    }
}